module "security-group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "5.1.2"

  name = "public_bastion_sg"
  description = "this is the web sg"
  vpc_id = module.vpc.vpc_id 
  ingress_rules = [ "ssh-tcp","http-80-tcp" ]
  ingress_cidr_blocks = [module.vpc.vpc_cidr_block]
  egress_rules = ["all-all"]
  tags = local.comom_tags
}