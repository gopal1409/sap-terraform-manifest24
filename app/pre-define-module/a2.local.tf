locals {
  owners = var.business_devision
  environment = var.environment
  name = "${var.business_devision}-${var.environment}"

  comom_tags = {
    owners = local.owners
    environment = local.environment
  }
}