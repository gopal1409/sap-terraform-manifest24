output "vpc_id" {
  value = aws_vpc.main.id
}


output "public_subnet_id" {
  value = toset([for subnet in aws_subnet.public_subnet : subnet.id])
}


output "private_subnet_id" {
  value = toset([for subnet in aws_subnet.private_subnet : subnet.id])
}


# DB - Subnet Group

output "db_subnet_id" {
  value = toset([for subnet in aws_subnet.database_subnet : subnet.id])
}
